package com.morten242.assignment1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;

/**
 * Activity for displaying and letting you choose element to load from database
 * Gave me some weird theme.appcompat error when fragment, so it's not a fragment
 * @author Mårten Nordheim
 */
public class RecordsActivity extends Activity {

    private ListView listView;
    LocationInfoDbHelper dbHelper;
    ArrayList<Long> ids;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_records);

        dbHelper = new LocationInfoDbHelper(this);

        setupUi();
        populateListView();
    }

    /**
     * Populate the ListView with IDs from the database
     */
    private void populateListView() {
        ids = dbHelper.getListOfIds();

        // For the list-items' labels we'll use the ID
        // Crappy UX - Check
        ArrayList<String> list = new ArrayList<>();
        for (long id : ids){
            list.add(Long.toString(id));
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                list);
        listView.setAdapter(arrayAdapter);
    }

    /**
     * Get references to and set up the UI elements
     */
    private void setupUi() {
        listView = (ListView) findViewById(R.id.db_list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Long idFromItem = ids.get(position);
                LocationInfo locationInfo = dbHelper.getLocationInfoById(idFromItem);

                Intent intent = new Intent(RecordsActivity.this, InfoActivity.class);
                intent.putExtra(LocationInfo.INTENT_LOCATION_INFO, locationInfo);
                startActivity(intent);
            }
        });
    }


}
