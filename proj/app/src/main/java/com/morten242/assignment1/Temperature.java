package com.morten242.assignment1;

import java.io.Serializable;

/**
 * A class for temperature
 * @author Mårten Nordheim (120367)
 */
public class Temperature implements Serializable{

    // Stored as celsius by default
    private double temperature;

    /**
     * Constructor for temperature
     * @param temperature the temperature
     * @param isCelsius if it's not celsius then it's fahrenheit, no choice
     */
    public Temperature(double temperature, boolean isCelsius) {
        setTemperature(temperature, isCelsius);
    }

    /**
     * Returns the temperature in celsius
     * @return the temperature in celsius
     */
    public double getCelsius(){
        return temperature;
    }

    /**
     * Returns the temperature in fahrenheit
     * @return the temperature in fahrenheit
     */
    public double getFahrenheit(){
        // Since the temperature is stored as celsius we need to convert
        // to fahrenheit
        return temperature * 9 / 5 + 32;
    }

    public void setTemperature(double temperature, boolean isCelsius) {
        if (isCelsius) {
            this.temperature = temperature;
        }
        else {
            this.temperature = (temperature - 32) * 5 / 9;
        }
    }
}
