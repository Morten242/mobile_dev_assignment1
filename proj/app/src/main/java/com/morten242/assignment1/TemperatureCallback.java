package com.morten242.assignment1;

/**
 * Interface for temperature callback
 * @author Mårten Nordheim (120367)
 */
interface TemperatureCallback {
    /**
     * Gets called when temperature is retrieved
     * @param temperature the temperature
     */
    void onTemperatureFinished(Temperature temperature);
}
