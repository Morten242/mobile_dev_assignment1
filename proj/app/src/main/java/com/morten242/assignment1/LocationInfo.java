package com.morten242.assignment1;

import android.location.Address;
import android.location.Location;
import java.io.Serializable;

/**
 * A class for storing location info.
 * Is serializable so I can pass it to other activities using Intent.
 * @author Mårten Nordheim (120367)
 * @see android.content.Intent
 */
public class LocationInfo implements Serializable, TemperatureCallback {
    private static final long serialVersionUID = 1L;
    // For an easily accessible key for passing this class through intent
    public static final String INTENT_LOCATION_INFO = "INTENT_LOCATION_INFO";
    private LatitudeLongitude location = null;
    private boolean hasAltitude = true;
    private Double altitude = null;
    private String locationName;
    private boolean fetchingTemperature = false;
    private Temperature temperature;
    private boolean backedInDb = false;

    /**
     * Constructor for LocationInfo
     */
    public LocationInfo() {
    }

    /**
     * Return the latitude
     * @return the latitude
     */
    public Double getLatitude() {
        if (location == null){
            return null;
        }
        return location.getLatitude();
    }

    /**
     * Returns the longitude
     * @return the longitude
     */
    public Double getLongitude() {
        if (location == null){
            return null;
        }
        return location.getLongitude();
    }

    /**
     * Calling this function will set a new latitude, longitude, altitude, temperature and location name.
     * Temperature will be fetched asynchronously using the Weather
     * @param l the location
     * @param address the address
     */
    public void updateLocation(Location l, Address address, Weather weather) {
        backedInDb = false;
        this.location = new LatitudeLongitude(l.getLatitude(), l.getLongitude());

        hasAltitude = l.hasAltitude();
        altitude = l.getAltitude();

        weather.getTemperatureAsync(this.location, this);
        fetchingTemperature = true;

        if (address != null) {
            this.locationName = address.getLocality();
        } else {
            locationName = null;
        }
    }

    /**
     * Returns the air temperature in celsius
     * @return the air temperature in celsius
     */
    public synchronized Double getAirTemperature() {
        if (temperature == null){
            return null;
        }
        return temperature.getCelsius();
    }

    /**
     * Returns the air temperature in celsius, in a formatted string
     * Example: 22C
     * @return the air temperature in celsius, in a formatted string
     */
    public String getAirTemperatureString()  {
        Double airTemp;
        airTemp = getAirTemperature();
        if (airTemp == null){
            return null;
        }
        return String.format("%sC", airTemp);
    }

    /**
     * Returns the altitude
     * @return the altitude
     */
    public Double getAltitude() {
        return altitude;
    }

    /**
     * Tells whether or not the current device supports altitude
     * @return true if altitude is supported, false if not
     */
    public boolean hasAltitude(){
        return hasAltitude;
    }

    /**
     * Returns the name of a location
     * @return the name of a location
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * Returns whether or not it is waiting for a callback about temperature
     * @return whether or not it is waiting for a callback about temperature
     */
    public boolean isFetchingTemperature() {
        return fetchingTemperature;
    }

    @Override
    public synchronized void onTemperatureFinished(Temperature temperature) {
        fetchingTemperature = false;
        this.temperature = temperature;
    }

    /**
     * Sets the latitude and longitude
     * @param location the new latitude and longitude
     */
    public void setLocation(LatitudeLongitude location) {
        this.location = location;
    }

    /**
     * Sets the altitude
     * @param altitude the new altitude
     */
    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    /**
     * Sets the location name
     * @param locationName the new location name
     */
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    /**
     * Sets the temperature
     * @param temperature the new temperature
     */
    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }


    /**
     * Returns whether or not this instance has been backed up in the database
     * @return whether or not this instance has been backed up in the database
     */
    public boolean isBackedInDb() {
        return backedInDb;
    }

    /**
     * Manually set whether or not this has been backed up in the database
     * (sorta hacky)
     * @param backedInDb is backed in database?
     */
    public void setBackedInDb(boolean backedInDb) {
        this.backedInDb = backedInDb;
    }
}
