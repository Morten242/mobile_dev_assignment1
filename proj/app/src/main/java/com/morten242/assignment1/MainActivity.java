package com.morten242.assignment1;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Locale;

/**
 * Main class
 * @author Mårten Nordheim (120367)
 */
public class MainActivity extends Activity implements OnMapReadyCallback,
            GoogleApiClient.ConnectionCallbacks,
            GoogleApiClient.OnConnectionFailedListener {

    private static final int ZOOM = 10;
    private final String SAVED_LOCATION_INFO = "savedLocationInfo";
    private GoogleMap map;
    private GoogleApiClient googleApiClient;
    private LocationInfo locationInfo;
    private Button locateButton;
    private Marker marker = null;
    private LocationInfoDbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupUi();
        buildGoogleApiClient();

        dbHelper = new LocationInfoDbHelper(this);

        if (savedInstanceState != null) {
            locationInfo = (LocationInfo) savedInstanceState.getSerializable(SAVED_LOCATION_INFO);
        } else {
            LocationInfo mostRecent = dbHelper.loadMostRecent();
            if (mostRecent != null) {
                locationInfo = mostRecent;
            }
        }
    }

    /**
     * Gets "references" to UI elements
     */
    private void setupUi() {
        locateButton = (Button) findViewById(R.id.locate_button);
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // button to reset the database (mostly for testing)
        if (id == R.id.reset_database) {
             dbHelper.reset();
             locationInfo = null;
             return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A function that tells the googleApiClient to connect
     * @param view the view that activated this function
     */
    public void locateButtonClicked(View view) {
        if (googleApiClient.isConnected()) {
            runUpdateLocation();
        } else {
            // This is fine because the onConnected callback also does runUpdateLocation
            googleApiClient.connect();
        }
    }

    private void runUpdateLocation() {
        // Inspired by (/this one following line is taken from):
        // https://developer.android.com/training/location/retrieve-current.html
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(
                googleApiClient);
        if (lastLocation == null) {
            return;
        }
        updateLocationInfo(lastLocation);
        affectMap(locationInfo);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        locateButton.setEnabled(true);
        map = googleMap;
        if (locationInfo != null){
            affectMap(locationInfo);
        }
    }

    /**
     * Adds hooks and such to API and callbacks.
     */
    private synchronized void buildGoogleApiClient() {
        // Taken from https://developer.android.com/training/location/retrieve-current.html
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        runUpdateLocation();
    }

    /**
     * Updates a private field of type LocationInfo with new info.
     * Address and weather for LocationInfo's constructor is defined in this function.
     * @param location the location
     * @see LocationInfo
     */
    private void updateLocationInfo(Location location) {
        Address address = getAddress(location);
        Yr yr = null;
        try {
            yr = new Yr();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (locationInfo == null) {
            locationInfo = new LocationInfo();
        } else {
            dbHelper.saveToDb(locationInfo);
        }
        locationInfo.updateLocation(location, address, yr);
    }

    /**
     * Gets an Address based on a location
     * @param location the location
     * @return the Address of the location
     */
    private Address getAddress(Location location) {
        // Based on / taken from / written off of
        // http://karanbalkar.com/2013/11/tutorial-63-implement-reverse-geocoding-in-android/
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);

        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(),
                    location.getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return addresses == null ? null : addresses.get(0);
    }

    /**
     * For the lack of a better name it's called affectMap.
     * It does things to the map.
     * @param locationInfo the location info
     */
    private void affectMap(LocationInfo locationInfo) {
        final LatLng latLng = new LatLng(locationInfo.getLatitude(), locationInfo.getLongitude());
        updateMapMarker(latLng);
        moveMapCamera(latLng);
    }

    /**
     * It moves the camera to the position
     * @param latLng the latitude and longitude
     */
    private void moveMapCamera(final LatLng latLng) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, ZOOM);
        map.moveCamera(cameraUpdate);
    }

    /**
     * Updates a marker on the map
     * If the marker does not exist it will create one
     * @param latLng the latitude and longitude
     */
    private void updateMapMarker(final LatLng latLng) {
        if (marker == null) {
            marker = map.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(getString(R.string.your_position_text)));
        } else {
            marker.setPosition(latLng);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.w("GoogleAPIConnection", "Suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.w("GoogleAPIConnection",
                String.format("Failed to connect to google api:  %s",
                        connectionResult.toString()));
    }

    /**
     * Passes location info to and starts the information-screen activity
     * @param view the view that activated this function
     */
    public void infoButtonClicked(View view) {
        if (locationInfo == null) {
            locationInfo = new LocationInfo();
        }
        Intent intent = new Intent(this, InfoActivity.class);
        intent.putExtra(LocationInfo.INTENT_LOCATION_INFO, locationInfo);
        startActivity(intent);
    }

    @Override
    public void onStop(){
        super.onStop();
        if (locationInfo != null) {
            dbHelper.saveToDb(locationInfo);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        savedInstanceState.putSerializable(SAVED_LOCATION_INFO, locationInfo);
        super.onSaveInstanceState(savedInstanceState);
    }

    /**
     * Opens the Records activity
     * @param view the view
     */
    public void recordsButtonClicked(View view) {
        Intent intent = new Intent(this, RecordsActivity.class);
        startActivity(intent);
    }
}
