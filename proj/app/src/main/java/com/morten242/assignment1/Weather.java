package com.morten242.assignment1;

import android.os.AsyncTask;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * An abstract class for weather API services
 * @author Mårten Nordheim (120367)
 */
abstract class Weather {
    final protected URL urlBase;
    final private String serviceName;

    /**
     * Constructor for Weather
     */
    Weather(URL url, String service){
        urlBase = url;
        serviceName = service;
    }

    /**
     * Get temperature for an Address.
     * @param latLong the latitude and longitude
     * @param temperatureCallback the class to notified when temperature has been retrieved
     * @see Temperature
     * @return the task it starts
     */
    abstract public AsyncTask getTemperatureAsync(LatitudeLongitude latLong, TemperatureCallback temperatureCallback);

    /**
     * Returns the name of the provider
     * @return the name of the provider
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Returns the url to the website providing the API
     * @return the url to the website providing the API
     */
    public URL getUrl() {
        return urlBase;
    }

    /**
     * Opens a connection to a URL and returns the stream
     * @param url the URL
     * @return InputStream to the content on url
     * @throws IOException when the protocol is not http
     */
    protected InputStream openHTTPConnection(URL url) throws IOException {
        // Taken from (slightly edited)
        // https://bitbucket.org/gtl-hig/imt3662_file_download/src/e600d3d18f2a2cb7343b2abd3b081f232c009fbc/src/no/hig/imt3662/filedownload/MainActivity.java?at=master&fileviewer=file-view-default#MainActivity.java-284
        InputStream inputStream = null;
        URLConnection urlConnection = url.openConnection();

        if (!(urlConnection instanceof HttpURLConnection)){
            throw new IOException("Not an HTTP connection");
        }

        HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
        httpURLConnection.setAllowUserInteraction(false);
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setRequestMethod("GET");
        httpURLConnection.connect();

        int response = httpURLConnection.getResponseCode();
        if (response == HttpURLConnection.HTTP_OK){
            inputStream = httpURLConnection.getInputStream();
        }
        return inputStream;
    }
}
