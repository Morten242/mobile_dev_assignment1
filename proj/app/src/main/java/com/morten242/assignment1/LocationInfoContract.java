package com.morten242.assignment1;

import android.provider.BaseColumns;

// Widely based on
// https://developer.android.com/training/basics/data-storage/databases.html

/**
 * Layout and some helper things for database
 * @author Mårten Nordheim (120367)
 */
public final class LocationInfoContract {
    // avoid instantiating
    public LocationInfoContract(){}

    /**
     * Defining an entry
     * @author Mårten Nordheim (120367)
     */
    public static abstract class LocationInfoEntry implements BaseColumns {
        public static final String TABLE_NAME = "locationinfodb";
        public static final String COLUMN_NAME_LATITUDE = "latitude"; // double (real)
        public static final String COLUMN_NAME_LONGITUDE = "longitude"; // double (real)
        public static final String COLUMN_NAME_ALTITUDE = "altitude"; // double (real)
        public static final String COLUMN_NAME_LOCATION_NAME = "locationname"; // string
        public static final String COLUMN_NAME_TEMPERATURE = "temperature"; // int
    }

}
