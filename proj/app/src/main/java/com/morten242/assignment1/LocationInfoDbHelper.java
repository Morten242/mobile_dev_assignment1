package com.morten242.assignment1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import java.util.ArrayList;

import static com.morten242.assignment1.LocationInfoContract.LocationInfoEntry.COLUMN_NAME_ALTITUDE;
import static com.morten242.assignment1.LocationInfoContract.LocationInfoEntry.COLUMN_NAME_LATITUDE;
import static com.morten242.assignment1.LocationInfoContract.LocationInfoEntry.COLUMN_NAME_LOCATION_NAME;
import static com.morten242.assignment1.LocationInfoContract.LocationInfoEntry.COLUMN_NAME_LONGITUDE;
import static com.morten242.assignment1.LocationInfoContract.LocationInfoEntry.COLUMN_NAME_TEMPERATURE;
import static com.morten242.assignment1.LocationInfoContract.LocationInfoEntry.TABLE_NAME;
import static com.morten242.assignment1.LocationInfoContract.LocationInfoEntry._ID;

/**
 * Implementation of SQLiteOpenHelper
 * Based on Google's tutorial:
 * https://developer.android.com/training/basics/data-storage/databases.html
 * @author Mårten Nordheim (120367)
 */
public class LocationInfoDbHelper extends SQLiteOpenHelper {

    // Helper-statements
    private static final String TEXT_TYPE = " TEXT";
    private static final String DOUBLE_TYPE = " REAL";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + INTEGER_TYPE + " PRIMARY KEY NOT NULL" + COMMA_SEP +
                COLUMN_NAME_LATITUDE + DOUBLE_TYPE + COMMA_SEP +
                COLUMN_NAME_LONGITUDE + DOUBLE_TYPE + COMMA_SEP +
                COLUMN_NAME_ALTITUDE + DOUBLE_TYPE + COMMA_SEP +
                COLUMN_NAME_LOCATION_NAME + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_TEMPERATURE + DOUBLE_TYPE + ")";
    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    // Database info
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "LocationInfo.db";

    // A string-array which lists all columns. To be used in queries
    private final String[] ALL_COLUMNS_PROJECTION = new String[]{
            COLUMN_NAME_LATITUDE,
            COLUMN_NAME_LONGITUDE,
            COLUMN_NAME_ALTITUDE,
            COLUMN_NAME_LOCATION_NAME,
            COLUMN_NAME_TEMPERATURE
    };

    /**
     * constructor for LocationInfoDbHelper
     * @param context a Context
     */
    public LocationInfoDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    /**
     * Returns a list of the IDs in the table for LocationInfo
     * @return a list of the IDs in the table for LocationInfo
     */
    public ArrayList<Long> getListOfIds(){
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query(TABLE_NAME,
                new String[]{_ID}, // I want ID
                null,
                null,
                null,
                null,
                _ID + " ASC",      // Sorted ascending
                null);

        ArrayList<Long> list = new ArrayList<>();

        final int columnIndex = c.getColumnIndex(_ID);
        if (c.moveToFirst()) {
            do {
                list.add(c.getLong(columnIndex));
            } while (c.moveToNext());
        }

        c.close();
        db.close();
        return list;
    }

    /**
     * Returns an instance of LocationInfo based on its ID in the database
     * @param id the entry's ID in the database
     * @return an instance of LocationInfo
     */
    public LocationInfo getLocationInfoById(long id) {
        SQLiteDatabase db = getReadableDatabase();

        String[] matchStrings = {Long.toString(id)};
        Cursor c = db.query(TABLE_NAME,
                ALL_COLUMNS_PROJECTION,
                _ID + "=?",     // Looking for _ID equal to
                matchStrings,   // matchStrings
                null,
                null,
                null);

        if (c.getCount() <= 0){
            return null;
        }

        c.moveToFirst();

        LocationInfo locationInfo = getLocationInfo(c);

        c.close();
        db.close();
        return locationInfo;
    }

    /**
     * Returns the LocationInfo object stored where the cursor is pointing
     * @param c A cursor pointing at one entry
     * @return the LocationInfo object stored where the cursor is pointing
     * @throws IllegalArgumentException if 'c' is not pointed at anything
     */
    @NonNull
    private LocationInfo getLocationInfo(Cursor c) {
        if (c.getPosition() == -1){
            throw new IllegalArgumentException("Cursor has to point to something");
        }

        double latitude = c.getDouble(
                c.getColumnIndex(COLUMN_NAME_LATITUDE)
        );
        double longitude = c.getDouble(
                c.getColumnIndex(COLUMN_NAME_LONGITUDE)
        );
        Double altitude = c.getDouble(
                c.getColumnIndex(COLUMN_NAME_ALTITUDE)
        );
        String locationName = c.getString(
                c.getColumnIndex(COLUMN_NAME_LOCATION_NAME)
        );
        Double temperature = c.getDouble(
                c.getColumnIndex(COLUMN_NAME_TEMPERATURE)
        );

        LocationInfo locationInfo = new LocationInfo();
        locationInfo.setLocation(new LatitudeLongitude(latitude, longitude));
        locationInfo.setAltitude(altitude);
        locationInfo.setLocationName(locationName);
        locationInfo.setTemperature(new Temperature(temperature, true));
        locationInfo.setBackedInDb(true);
        return locationInfo;
    }

    /**
     * Delete the database and make a new one
     */
    public void reset() {
        SQLiteDatabase db = getWritableDatabase();
        onUpgrade(db, DATABASE_VERSION, DATABASE_VERSION);
        db.close();
    }

    /**
     * Returns the most recent entry of LocationInfo in the database
     * @return the most recent entry of LocationInfo in the database
     */
    public LocationInfo loadMostRecent(){
        SQLiteDatabase db = getReadableDatabase();

        // Sort by descending ID to the get latest entry first
        Cursor c = db.query(TABLE_NAME,
                ALL_COLUMNS_PROJECTION,
                null,
                null,
                null,
                null,
                _ID + " DESC",      // Sorted descending
                "1");               // But only 1 entry

        if (c.getCount() <= 0){
            c.close();
            db.close();
            return null;
        }

        // First entry wil be the latest / most recent, so that's the one we want
        c.moveToFirst();
        LocationInfo locationInfo = getLocationInfo(c);

        c.close();
        db.close();
        return locationInfo;
    }

    /**
     * Saved an instance of LocationInfo to the database
     * @param locationInfo the instance to store
     */
    public void saveToDb(LocationInfo locationInfo){
        if (locationInfo.isBackedInDb()) {
            return;
        }
        locationInfo.setBackedInDb(true);
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_LATITUDE, locationInfo.getLatitude());
        values.put(COLUMN_NAME_LONGITUDE, locationInfo.getLongitude());
        values.put(COLUMN_NAME_ALTITUDE, locationInfo.getAltitude());
        values.put(COLUMN_NAME_LOCATION_NAME, locationInfo.getLocationName());
        values.put(COLUMN_NAME_TEMPERATURE, locationInfo.getAirTemperature());

        db.insert(TABLE_NAME, null, values);
        db.close();
    }
}
