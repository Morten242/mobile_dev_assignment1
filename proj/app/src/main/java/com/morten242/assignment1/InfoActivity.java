package com.morten242.assignment1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Class for information screen
 * @author Mårten Nordheim
 */
public class InfoActivity extends Activity {

    private LocationInfo locationInfo;
    private TextView textLatitude;
    private TextView textLongitude;
    private TextView textAltitude;
    private TextView textPlace;
    private TextView textTemperature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        Intent intent = getIntent();
        locationInfo = (LocationInfo) intent.getSerializableExtra(LocationInfo.INTENT_LOCATION_INFO);
        setupUi();
    }

    private void setupUi() {
        textLatitude = (TextView) findViewById(R.id.text_latitude);
        textLongitude = (TextView) findViewById(R.id.text_longitude);
        textAltitude = (TextView) findViewById(R.id.text_altitude);
        textPlace = (TextView) findViewById(R.id.text_place);
        textTemperature = (TextView) findViewById(R.id.text_temperature);

        setupText();
    }

    private void setupText() {
        setupLatLngText();
        setupAltitudeText();
        setupPlaceText();
        setupTemperatureText();
    }

    private void setupPlaceText() {
        String locationName;
        String s = locationInfo.getLocationName();
        if (s == null) {
            locationName = String.format(getString(R.string.value_has_not_been_retrieved), "Location name");
        } else {
            locationName = String.format(getString(R.string.you_are_in_string), s);
        }
        textPlace.setText(locationName);
    }

    private void setupTemperatureText() {
        String temperature;
        String temperatureString = locationInfo.getAirTemperatureString();
        if (temperatureString == null) {
            if (locationInfo.isFetchingTemperature()){
                temperature = getString(R.string.currently_fetching_temperature);
            } else {
                temperature = String.format(getString(R.string.value_has_not_been_retrieved), "Temperature");
            }

        } else {
            temperature = String.format(getString(R.string.text_temperature_format),
                    temperatureString);
        }
        textTemperature.setText(temperature);
    }

    private void setupAltitudeText() {
        Double altitudeValue = locationInfo.getAltitude();
        String altitude;
        if (!locationInfo.hasAltitude()) {
            altitude = getString(R.string.altitude_not_supported);
        } else {
            if (altitudeValue == null) {
                altitude = String.format(getString(R.string.value_has_not_been_retrieved), "Altitude");
            } else {
                altitude = String.format(getString(R.string.text_altitude_format), altitudeValue);
            }
        }
        textAltitude.setText(altitude);
    }

    private void setupLatLngText() {
        Double latitudeValue = null;
        Double longitudeValue = null;
        try {
            latitudeValue = locationInfo.getLatitude();
            longitudeValue = locationInfo.getLongitude();
        } catch (Exception ignored) {
        }

        String latitude;
        String longitude;
        // Printing only if both have been retrieved because:
        // 1. It's unlikely that only one succeeded (should be impossible)
        // 2. One without the other is useless
        if (latitudeValue == null || longitudeValue == null) {
            latitude = String.format(getString(R.string.value_has_not_been_retrieved), "Latitude");
            longitude = String.format(getString(R.string.value_has_not_been_retrieved), "Longitude");
        } else {
            latitude = String.format(getString(R.string.text_latitude_format), latitudeValue.doubleValue());
            longitude = String.format(getString(R.string.text_longitude_format), longitudeValue.doubleValue());
        }
        textLatitude.setText(latitude);
        textLongitude.setText(longitude);
    }
}
