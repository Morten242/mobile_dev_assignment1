package com.morten242.assignment1;

import android.os.AsyncTask;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * A class for getting weather info (temperature) from Yr.no
 * Inherits from Weather
 * @see Weather
 * @author Mårten Nordheim (120367)
 */
public class Yr extends Weather {


    /**
     * Constructor for Yr
     */
    Yr() throws MalformedURLException {
        super(new URL("http://www.yr.no"), "Yr");
    }

    @Override
    public String getServiceName() {
        return "Yr";
    }

    /**
     * Get temperature for an Address. The address needs to be using an english locale
     * @param latLong the latitude and longitude
     * @param temperatureCallback the class to notified when temperature has been retrieved
     * @see Temperature
     * @return the task it starts
     */
    @Override
    public AsyncTask getTemperatureAsync(LatitudeLongitude latLong, TemperatureCallback temperatureCallback) {
        URL url = null;
        try {
            url = getFormattedURL(latLong);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if (url != null) {
            return new GetTemperatureFromXMLTask(temperatureCallback).execute(url);
        }
        return null;
    }

    /**
     * A class to get the temperature asynchronously
     */
    private class GetTemperatureFromXMLTask extends AsyncTask<URL, Void, Temperature> {
        // Based on
        // https://bitbucket.org/gtl-hig/imt3662_file_download/src/e600d3d18f2a2cb7343b2abd3b081f232c009fbc/src/no/hig/imt3662/filedownload/MainActivity.java?at=master
        final TemperatureCallback temperatureCallback;
        boolean isCancelled = false;

        /**
         *  Constructor for GetTemperatureFromXMLTask
         * @param temperatureCallback the class to be notified when finished
         */
        public GetTemperatureFromXMLTask(TemperatureCallback temperatureCallback) {
            super();
            this.temperatureCallback = temperatureCallback;
        }

        @Override
        protected Temperature doInBackground(URL... params) {
            if (params.length != 1) {
                throw new IllegalArgumentException("Need exactly 1 argument");
            }
            return getTemperatureFromXML(params[0]);
        }

        @Override
        protected void onPostExecute(Temperature temperature){
            temperatureCallback.onTemperatureFinished(temperature);
        }

        @Override
        protected void onCancelled(){
            isCancelled = true;
        }

        /**
         * Gets the temperature from Yr's API
         * @param url the formatted URL
         * @return the current temperature
         */
        private Temperature getTemperatureFromXML(URL url) {
            // Based on
            // https://bitbucket.org/gtl-hig/imt3662_file_download/src/e600d3d18f2a2cb7343b2abd3b081f232c009fbc/src/no/hig/imt3662/filedownload/MainActivity.java?at=master&fileviewer=file-view-default#MainActivity.java-224
            String result = null;
            InputStream inputStream;
            Boolean isCelsius = null;
            try {
                inputStream = openHTTPConnection(url);
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db;
                Document doc = null;

                if (isCancelled) {
                    return null;
                }
                try {
                    db = dbf.newDocumentBuilder();
                    doc = db.parse(inputStream);
                } catch (ParserConfigurationException | SAXException e) {
                    e.printStackTrace();
                }

                if (doc == null || isCancelled){
                    return null;
                }

                doc.getDocumentElement().normalize();

                // Get the first <temperature> node, which is temperature for current time
                Node node = doc.getElementsByTagName("temperature").item(0);
                NamedNodeMap namedNodeMap = node.getAttributes();
                // See if the value is given in celsius, if not then Temperature needs to know
                isCelsius = namedNodeMap.getNamedItem("unit").getNodeValue().equals("celsius");
                result = namedNodeMap.getNamedItem("value").getNodeValue();

            } catch (IOException e) {
                e.printStackTrace();
            }

            if (result == null) {
                return null;
            }

            double temp = Double.parseDouble(result);
            return new Temperature(temp, isCelsius);
        }
    }

    /**
     * Given a Latitude and a Longitude, will return a URL to the XML for that area on yr.no.
     * API documentation can be found here:
     * http://api.yr.no/weatherapi/locationforecastlts/1.2/documentation
     * @param latLong the latitude and longitude
     * @return a URL to the XML for that area on yr.no
     * @throws MalformedURLException when the URL is invalid
     */
    private URL getFormattedURL(LatitudeLongitude latLong) throws MalformedURLException {
        final String URL_FORMAT = "http://api.yr.no/weatherapi/locationforecastlts/1.2/?lat=%s;lon=%s";

        // Need to be sure the decimal symbol is a period
        DecimalFormat df = new DecimalFormat("#.#", new DecimalFormatSymbols(Locale.US));

        String latitude = df.format(latLong.getLatitude());
        String longitude = df.format(latLong.getLongitude());

        String urlString = String.format(URL_FORMAT, latitude, longitude);
        return new URL(urlString);
    }
}
