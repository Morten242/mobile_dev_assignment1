package com.morten242.assignment1;

import java.io.Serializable;

/**
 * A class I made because LatLng isn't serializable.
 * @author Mårten Nordheim (120367)
 */
class LatitudeLongitude implements Serializable {
    private static final long serialVersionUID = 1L;
    private double latitude;
    private double longitude;

    /**
     * Constructor for LatitudeLongitude
     * @param latitude the latitude
     * @param longitude the longitude
     */
    public LatitudeLongitude(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * Returns the latitude
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Sets the latitude
     * @param latitude the new latitude
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * Returns the longitude
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Sets the longitude
     * @param longitude the new longitude
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
